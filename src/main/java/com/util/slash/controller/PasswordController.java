package com.util.slash.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.util.slash.modal.Request;
import com.util.slash.service.PasswordServ;

@RestController
@RequestMapping("v1/")
public class PasswordController {

	@Autowired
	private PasswordServ passwordServ;

	@GetMapping(value = "/isSame")
	@ResponseBody
	public boolean checkIfPasswordIsMatchingWithGluuHash(@RequestBody Request request){
		return passwordServ.isPasswordSameAsGluuHashedPassword(request);
	}
}
