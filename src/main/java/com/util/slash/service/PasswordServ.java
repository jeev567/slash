package com.util.slash.service;

import org.springframework.stereotype.Service;

import com.util.slash.exception.SlashException;
import com.util.slash.modal.Base;

@Service
public interface PasswordServ {

	public boolean isPasswordSameAsGluuHashedPassword(Base base) throws SlashException;
}
