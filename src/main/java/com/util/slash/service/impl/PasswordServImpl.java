package com.util.slash.service.impl;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;

import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Service;

import com.util.slash.exception.SlashException;
import com.util.slash.modal.Base;
import com.util.slash.modal.Request;
import com.util.slash.service.PasswordServ;
import com.util.slash.util.Constants;

@Service
public class PasswordServImpl implements PasswordServ {

	@Override
	public boolean isPasswordSameAsGluuHashedPassword(Base base) throws SlashException {
		final Request req = (Request) base;
		if (StringUtils.isBlank(req.getHash()) || (StringUtils.isBlank(req.getPassword()))) throw new SlashException("Hash and/or password cannot be blank");

		return isGluuHashPasswordSame(req.getHash(), req.getPassword());
	}

	/**
	 *
	 * @param oldPasswordHash
	 * @param oldPassword
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 */
	public boolean isGluuHashPasswordSame(String oldPasswordHash, String oldPassword) {

		try {
			Security.insertProviderAt(new org.bouncycastle.jce.provider.BouncyCastleProvider(), 1);
			oldPasswordHash = oldPasswordHash.replace(Constants.UCS_CRYPTO_BASE64_BINARY_TOKEN.toString(), "");
			oldPasswordHash = oldPasswordHash.replace(new String(Base64.encodeBase64(Constants.UCS_CRYPTO_SHA_512_TOKEN.toString().getBytes())), "");

			final byte[][] hs = split(Base64.decodeBase64(new String(Base64.decodeBase64(oldPasswordHash))), 64);
			final byte[] hash = hs[0];
			final byte[] salt = hs[1];
			if (new String(Base64.encodeBase64(hash)).equals(hashStringWithSalt(oldPassword, salt, Constants.UCS_CRYPTO_ALGORITHM.SHA_512.getAlgorithm()))) return true;

		} catch (final Exception e) {
			throw new SlashException("Error occured while checking if gluu hashed password is same as current password: " + e.getLocalizedMessage());
		}
		return false;
	}

	private static byte[][] split(byte[] src, int n) {
		byte[] l, r;
		if (src == null || src.length <= n) {
			l = src;
			r = new byte[0];
		} else {
			l = new byte[n];
			r = new byte[src.length - n];
			System.arraycopy(src, 0, l, 0, n);
			System.arraycopy(src, n, r, 0, r.length);
		}
		final byte[][] lr = {l, r};
		return lr;
	}

	private static String hashStringWithSalt(String input, byte[] salt, String cryptoAlgorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
		final MessageDigest sha = MessageDigest.getInstance(cryptoAlgorithm, "BC");
		sha.reset();
		sha.update(input.getBytes(StandardCharsets.UTF_8));
		sha.update(salt);
		final byte[] pwhash = sha.digest();
		return new String(Base64.encodeBase64(pwhash));
	}
}
