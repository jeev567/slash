package com.util.slash.util;

/**
 *
 * @author Jeevan Abraham
 * @description The class is responsible for centralizing and avoiding hard
 *              coding values within other java classes
 */
public interface Constants {
	String ADAP_REQUEST_ROOT_NAME = "params";
	String ADAP_RESPONSE_ROOT_NAME = "resources";
	String ADAP_RESPONSE_WRAPPER_NAME = "response";

	String ADAP_OU = "ou";
	String ADAP_DC = "dc";

	String REST_USER_ATTRIBUTE_REQUEST_TOP = "top";
	String REST_USER_ATTRIBUTE_REQUEST_PERSON = "person";
	String REST_USER_ATTRIBUTE_REQUEST_ORG_PERSON = "organizationalPerson";
	String REST_USER_ATTRIBUTE_REQUEST_INET_ORG_PERSON = "inetOrgPerson";
	String REST_USER_ATTRIBUTE_REQUEST_INMOMENT_USER = "inmomentuser";

	String ADAP_PASSWORD_POLICY_USER_TYPE = "cn=Password Policy,cn=config";

	String REST_HTTP_STATUS_OK = "200";

	enum ADAP_MODIFY_TYPE {
		REPLACE, TBD, TBD1
	}

	enum ADAP_MODIFY_ATTRIBUTES {
		givenname, sn, mail, userpassword , pwdReset
	}

	String SWAGGER_SPEC_TITLE = "User Provisioning Service";
	String SWAGGER_SPEC_DESCRIPTION = "UPS is a process in Identity and Access Management which ensures that user accounts are automatically created, modified, searched and deleted";
	String SWAGGER_SPEC_VERSION = "1.0";
	String SWAGGER_SPEC_CONTACT_URL = "https://www.inmoment.com/";
	String SWAGGER_SPEC_CONTACT_NAME = "InMoment";
	String SWAGGER_SPEC_CONTACT_EMAIL = "jabraham@inmoment.com";

	String ADAP_TIMESTAMP_FORMAT = "yyyyMMddHHmmss.SSSZ";
	String APOLLO_TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

	public enum UCS_CRYPTO_ALGORITHM {
		SHA_512("SHA-512"), SHA_256("SHA-256");
		private final String algo;

		UCS_CRYPTO_ALGORITHM(String algo) {
			this.algo = algo;
		}

		public String getAlgorithm() {
			return this.algo;
		}
	};

	String UCS_CRYPTO_SHA_512_TOKEN= "{SSHA512}";
	String UCS_CRYPTO_BASE64_BINARY_TOKEN= "{base64binary}";

	String DOMAIN_PATH_V1= "/v1";

}
